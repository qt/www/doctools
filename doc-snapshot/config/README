Qt 5 Online Templates
=====================

The doctools/doc-snapshot/config directory contains the style and templates to
build the documentation as seen on doc-snapshot.qt-project.org. There are
templates for each of the qt5.git branches: dev, stable, and release.
Additionally, the templates dictate that QDoc output the HTML files into
one directory.

These templates are valid from Qt 5.2 and onwards.

To use:
======
1. Copy the contents of doctools/doc-snapshot/config to qtbase/doc/global.
   Note that the subdirectories structure is necessary.

2. Replace the qtbase/doc/global/config.qdocconf with either:

   a) config-dev.qdocconf - dev branch configuration
   b) config-stable.qdocconf - stable branch configuration
   c) config-release.qdocconf - release branch configuration
   d) config-53.qdocconf - 5.3 branch configuration

3. Replace qt-module-defaults.qdocconf with qt-module-defaults-snapshot.qdocconf

4. Run the regular "make docs" or "make install_docs" command.
   Note that the output is in the "html" directory, by default.

Practically, this is the sequence of commands (for stable branch):

    $ cd $QTSRC/qtbase/doc/global
    $ cp -r $BUILD/doctools/doc-snapshot/config/* .
    $ cp qt-module-defaults-snapshot.qdocconf qt-module-defaults.qdocconf
    $ cp config-stable.qdocconf config.qdocconf
